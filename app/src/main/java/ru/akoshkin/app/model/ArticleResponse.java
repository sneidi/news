package ru.akoshkin.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleResponse implements Parcelable {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("totalResults")
    @Expose
    private Integer totalResults;
    @SerializedName("articles")
    @Expose
    private List<Article> articles = null;

    public String getStatus() {
        return status;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public List<Article> getArticles() {
        return articles;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeValue(this.totalResults);
        dest.writeTypedList(this.articles);
    }

    public ArticleResponse() {
    }

    protected ArticleResponse(Parcel in) {
        this.status = in.readString();
        this.totalResults = (Integer) in.readValue(Integer.class.getClassLoader());
        this.articles = in.createTypedArrayList(Article.CREATOR);
    }

    public static final Parcelable.Creator<ArticleResponse> CREATOR = new Parcelable.Creator<ArticleResponse>() {
        @Override
        public ArticleResponse createFromParcel(Parcel source) {
            return new ArticleResponse(source);
        }

        @Override
        public ArticleResponse[] newArray(int size) {
            return new ArticleResponse[size];
        }
    };
}
