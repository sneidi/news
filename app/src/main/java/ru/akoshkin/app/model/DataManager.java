package ru.akoshkin.app.model;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.akoshkin.app.i.IApi;
import ru.akoshkin.app.i.IModel;
import ru.akoshkin.app.i.IPresenter;
import ru.akoshkin.app.utils.Constants;

public class DataManager implements IModel {
    private IPresenter presenter;

    public DataManager(IPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getArticles(int page) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        //httpClient.addInterceptor(logging);

        new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build().create(IApi.class)
                .getEveryThing(Constants.API_Q, Constants.API_FROM, Constants.API_SORTBY, Constants.API_KEY, page)
                .enqueue(new Callback<ArticleResponse>() {
                    @Override
                    public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                        if (response.isSuccessful() && response.body() != null && response.body().getArticles() != null) {
                            presenter.showData((ArrayList<Article>) response.body().getArticles());
                        } else onFailure(call, new Throwable());
                    }

                    @Override
                    public void onFailure(Call<ArticleResponse> call, Throwable t) {
                        presenter.showError();
                    }
                });
    }

}
