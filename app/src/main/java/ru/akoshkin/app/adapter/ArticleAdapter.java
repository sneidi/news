package ru.akoshkin.app.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ru.akoshkin.app.R;
import ru.akoshkin.app.i.ArticleListener;
import ru.akoshkin.app.model.Article;
import ru.akoshkin.app.view.ArticleViewHolder;
import ru.akoshkin.app.view.LoadingViewHolder;

public class ArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private ArrayList<Article> items;
    private ArticleListener listener;

    public ArticleAdapter(ArrayList<Article> items) {
        this.items = items;
    }

    public void setListener(ArticleListener listener) {
        this.listener = listener;
    }

    public void setItems(ArrayList<Article> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) == null) return VIEW_TYPE_LOADING;
        return VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_TYPE_ITEM)
            return new ArticleViewHolder(DataBindingUtil.inflate(inflater, R.layout.item_article, parent, false));
        else
            return new LoadingViewHolder(DataBindingUtil.inflate(inflater, R.layout.item_loading, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ArticleViewHolder) {
            Article article = items.get(position);
            ((ArticleViewHolder) viewHolder).bind(article);
            ((ArticleViewHolder) viewHolder).itemView.setOnClickListener(v -> listener.onArticleClick(article));
        } else {
            ((LoadingViewHolder) viewHolder).binding.btn.setOnClickListener(v -> {
                items.remove(position);
                notifyDataSetChanged();
                listener.onLoading();
            });
        }
    }


    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }


}
