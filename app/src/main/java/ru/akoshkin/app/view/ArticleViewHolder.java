package ru.akoshkin.app.view;

import androidx.recyclerview.widget.RecyclerView;

import ru.akoshkin.app.databinding.ItemArticleBinding;
import ru.akoshkin.app.model.Article;

public class ArticleViewHolder extends RecyclerView.ViewHolder {
    ItemArticleBinding binding;

    public ArticleViewHolder(ItemArticleBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Article article){
        binding.setArticle(article);
        binding.executePendingBindings();
    }
}
