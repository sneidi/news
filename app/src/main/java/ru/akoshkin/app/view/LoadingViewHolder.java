package ru.akoshkin.app.view;

import androidx.recyclerview.widget.RecyclerView;

import ru.akoshkin.app.databinding.ItemArticleBinding;
import ru.akoshkin.app.databinding.ItemLoadingBinding;

public class LoadingViewHolder extends RecyclerView.ViewHolder {

    public ItemLoadingBinding binding;
    public LoadingViewHolder(ItemLoadingBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
