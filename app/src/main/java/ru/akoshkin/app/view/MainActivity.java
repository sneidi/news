package ru.akoshkin.app.view;

import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import java.util.ArrayList;

import ru.akoshkin.app.R;
import ru.akoshkin.app.adapter.ArticleAdapter;
import ru.akoshkin.app.databinding.ActivityMainBinding;
import ru.akoshkin.app.i.IView;
import ru.akoshkin.app.model.Article;
import ru.akoshkin.app.utils.Constants;
import ru.akoshkin.app.utils.Presenter;


public class MainActivity extends AppCompatActivity implements IView, ArticleAdapter.ArticleListener {

    private ActivityMainBinding ui;

    private Presenter mPresenter;
    private ArticleAdapter mArticleAdapter;
    private ArrayList<Article> articles;

    CustomTabsIntent.Builder builder;
    CustomTabsIntent customTabsIntent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ui = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initUi();
    }

    private void initUi() {
        builder = new CustomTabsIntent.Builder();
        customTabsIntent = builder.build();
        builder.setShowTitle(true);
        builder.setToolbarColor(ContextCompat.getColor(this,R.color.colorPrimary));

        mPresenter = new Presenter(this);
        setSupportActionBar(ui.toolbar);

        articles = new ArrayList<>();
        mArticleAdapter = new ArticleAdapter(articles);
        mArticleAdapter.setListener(this);
        ui.recycler.setAdapter(mArticleAdapter);
        mPresenter.loadData();
    }

    @Override
    public void showErrorMsg() {

        Toast.makeText(this, Constants.MSG_ERROR, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(ArrayList<Article> articles) {
        this.articles.addAll(articles);
        mArticleAdapter.setItems(articles);
    }

    @Override
    public void onArticleClick(Article article) {
        if (article.getUrl() != null)
            customTabsIntent.launchUrl(this, Uri.parse(article.getUrl()));
        else showErrorMsg();
    }

    @Override
    public void onLoading() {
        mPresenter.loadData();
    }
}
