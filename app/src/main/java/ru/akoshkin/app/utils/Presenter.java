package ru.akoshkin.app.utils;

import java.util.ArrayList;

import ru.akoshkin.app.i.IModel;
import ru.akoshkin.app.i.IPresenter;
import ru.akoshkin.app.i.IView;
import ru.akoshkin.app.model.DataManager;
import ru.akoshkin.app.model.Article;

public class Presenter implements IPresenter {
    private IView mView;
    private IModel mModel;

    private int mPage;

    public Presenter(IView mView) {
        this.mView = mView;
        this.mModel = new DataManager(this);
        this.mPage = 1;
    }


    @Override
    public void loadData() {
        mModel.getArticles(mPage);
    }

    @Override
    public void showData(ArrayList<Article> articles) {
        ArrayList<Article> items = new ArrayList<>(articles);
        items.add(null);
        mView.showData(items);
        mPage++;
    }

    @Override
    public void showError() {
        mView.showErrorMsg();
    }
}
