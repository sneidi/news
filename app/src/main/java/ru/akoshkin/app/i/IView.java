package ru.akoshkin.app.i;

import java.util.ArrayList;

import ru.akoshkin.app.model.Article;

public interface IView {
    void showErrorMsg();
    void showData(ArrayList<Article> articles);
}
