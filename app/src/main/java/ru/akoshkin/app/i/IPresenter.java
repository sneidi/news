package ru.akoshkin.app.i;

import java.util.ArrayList;

import ru.akoshkin.app.model.Article;

public interface IPresenter {
    void loadData();
    void showData(ArrayList<Article> articles);
    void showError();
}
