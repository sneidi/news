package ru.akoshkin.app.i;

import ru.akoshkin.app.model.Article;

public interface ArticleListener {
    void onArticleClick(Article article);

    void onLoading();
}