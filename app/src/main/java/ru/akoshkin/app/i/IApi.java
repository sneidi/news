package ru.akoshkin.app.i;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.akoshkin.app.model.ArticleResponse;

public interface IApi {
    @GET("everything")
    Call<ArticleResponse> getEveryThing(
            @Query("q") String q,
            @Query("from") String from,
            @Query("sortby") String sortby,
            @Query("apiKey") String key,
            @Query("page") int page);
}
